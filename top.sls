base:
  '*':
    - epel
    - wget
    - java
    - firewall.rules
    - users
    - selinux
  'host:Jenkins*':
    - match: grain
    - Jenkins
  'role:blog*':
    - match: grain
    - test
    - apache
    - php
    - mysql
    - wordpress
    - nfs
    - mariadb
    - mariadb.client
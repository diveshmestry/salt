sudo  yum list installed | grep mysql-server

if [ $? -ne 0 ]
then
  sudo wget https://dev.mysql.com/get/mysql80-community-release-el7-3.noarch.rpm 2>&1
  sudo rpm -Uvh mysql80-community-release-el7-3.noarch.rpm 2>&1
  sudo yum-config-manager --enable mysql80-community && sudo yum update -y
  sudo yum install -y mysql-server
else
  echo "mysql-server already installed"
fi
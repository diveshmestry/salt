install_mysql_python:
  pkg.installed:
    - name: MySQL-python

install_mysql_php_extension:
  pkg.installed:
    - pkgs:
      - php-common
      - php-json
      - php-mysqlnd
      - php-pdo

install_mysql_server:
  cmd.script:
    - source: salt://mysql/install_mysql.sh
    - unless: sudo yum list installed | grep mysql*

start_mysql:
  service.running:
    - name: mysqld
    - require:
      - install_mysql_server

secure_mysql:
  cmd.script:
    - name: secure_db.sh "{{ pillar['root_password'] }}"
    - source: salt://mysql/secure_db.sh
    - require:
      - start_mysql
    - unless: mysql --user=root --password='{{ pillar['root_password'] }}' --execute="quit"

dmestry_p:
  mysql_user.present:
    - host: localhost
    - password: "{{ pillar['root_password'] }}"
    - connection_user: root
    - connection_pass: "{{ pillar['root_password'] }}"
    - connection_charset: utf8
    - require:
      - secure_mysql

ensure_db_present:
  mysql_database.present:
    - name: blog
    - connection_user: root
    - connection_pass: {{ pillar['root_password'] }}
    - require:
      - pkg: MySQL-python

mysql_user_grants:
   mysql_grants.present:
    - grant: all privileges
    - database: 'blog.*'
    - grant_option: true
    - user: 'dmestry_p'
    - host: 'localhost'
    - connection_user: root
    - connection_pass: {{ pillar['root_password'] }}
    - require:
      - ensure_db_present
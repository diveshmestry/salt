#! /bin/sh

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

#Functions

usage() {
cat << _EOF_

Usage: ${0} "ROOT PASSWORD"

  with "ROOT PASSWORD" the desired password for the database root user.

Use quotes if your password contains spaces or other special characters.
_EOF_
}

# check if the mysql(1) command is available,
# nonzero exit status otherwise.
is_mysql_command_available() {
  which mysql > /dev/null 2>&1
}

get_current_root_password(){
  sudo cat /var/log/mysqld.log | grep "A temporary password" | awk '{print $NF}'
}

alter_user_command(){
  #echo "ALTER USER 'root'@'localhost' IDENTIFIED BY '$1';"
  echo "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '$1';"

}

# check if new password is passed in the command line
if [ "$#" -ne "1" ]; then
  echo "Expected 1 argument, got $#" >&2
  usage
  exit 2
fi

#Variables
desired_db_root_password="${1}"
current_root_password=$(get_current_root_password)

# Script start
if ! is_mysql_command_available; then
  echo "The MySQL/MariaDB client mysql(1) is not installed."
  exit 1
fi

#echo "${current_root_password}"

#change the root password
if [ $? -eq 0 ]; then
  echo "changing password"
  alter_user=$(alter_user_command "${desired_db_root_password}")
  #echo "${alter_user}"
  mysql --connect-expired-password --user=root --password="${current_root_password}" --execute="${alter_user}" 2>&1
else
  echo "couldn't change password"
  exit 1
fi

#use new password to secure mysql
if [ $? -eq 0 ]; then
  echo "securing DB"
  mysql --user=root --password="${desired_db_root_password}" 2>&1 <<_EOF_
    DELETE FROM mysql.user WHERE User='';
    DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');
    DROP DATABASE IF EXISTS test;
    DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%';
    FLUSH PRIVILEGES;
_EOF_
else
  echo "couldn't secure DB"
  exit 1
fi

if [ $? -eq 0 ]; then
  echo "done"
fi

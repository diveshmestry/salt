get_jenkins_repo:
  cmd.run:
    - name: curl --silent --location http://pkg.jenkins-ci.org/redhat-stable/jenkins.repo | sudo tee /etc/yum.repos.d/jenkins.repos

install_jenkins_key:
  cmd.run:
    - name: sudo rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key

install_jenkins:
  pkg.installed:
    - name: jenkins

jenkins:
  service.running:
    - enable: True
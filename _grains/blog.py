#!/usr/bin/env python
def _setgrains():
    my_grains = {
        "role": "blog",
        "environment": "dev"
    }
    return my_grains


def main():
    # initialize a grains dictionary
    grains = {"my_grains": _setgrains()}
    return grains

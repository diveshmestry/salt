make_efs_utils_dir:
  file.directory:
    - name: /home/centos/efs_utils_git

clone-efs_utils-repo:
  cmd.run:
    - name: git clone https://github.com/aws/efs-utils /home/centos/efs_utils_git
    - creates: /home/centos/efs_utils_git/.git/
    - require:
      - make_efs_utils_dir

install_make:
  pkg.installed:
    - name: make

install_rpmbuild:
  pkg.installed:
    - name: rpm-build

install_efs_utils:
  cmd.run:
    - name: cd /home/centos/efs_utils_git && make rpm && sudo yum -y install ./build/amazon-efs-utils*rpm
    - creates: /home/centos/efs_utils_git/build/
    - require:
      - clone-efs_utils-repo

make_efs_dir:
  file.directory:
    - name: /var/www/html/efs

set_setbool_nfs:
  selinux.boolean:
    - name: httpd_use_nfs
    - value: True
    - persist: True
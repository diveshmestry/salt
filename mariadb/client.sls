install_mysql_python:
  pkg.installed:
    - name: MySQL-python

configure_mariadb_repo:
  file.managed:
    - name: /etc/yum.repos.d/MariaDB.repo
    - source: salt://mariadb/MariaDB.repo

install_mariadb_pkgs:
  pkg.installed:
    - pkgs:
      - MariaDB-client
      - MariaDB-server
    - require:
      - configure_mariadb_repo

ensure_mariadb_running:
  service.running:
    - name: mariadb
    - enable: True
    - require:
      - install_mariadb_pkgs

secure_mysql:
  cmd.script:
    - name: secure_db.sh "{{ pillar['root_password'] }}"
    - source: salt://mariadb/secure_db.sh
    - require:
      - ensure_mariadb_running
    - unless: mysql --user=root --password='{{ pillar['root_password'] }}' --execute="quit"

restart_minion_for_mysql:
  service.running:
    - name: salt-minion


dmestry_p:
  mysql_user.present:
    - host: {{  pillar['db_hostname']  }}
    - password: {{ pillar['db_password'] }}
    - connection_user: root
    - connection_pass: "{{ pillar['root_password'] }}"
    - connection_charset: utf8
    - require:
      - secure_mysql

mysql_user_grants:
   mysql_grants.present:
    - grant: all privileges
    - database: '*.*'
    - grant_option: true
    - user: {{  pillar['db_username']  }}
    - host: {{  pillar['db_hostname']  }}
    - connection_user: root
    - connection_pass: {{ pillar['root_password'] }}
    - require:
      - install_mariadb_pkgs
      - dmestry_p
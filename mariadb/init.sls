install_mysql_python:
  pkg.installed:
    - name: MySQL-python

configure_mariadb_repo:
  file.managed:
    - name: /etc/yum.repos.d/MariaDB.repo
    - source: salt://mariadb/MariaDB.repo

install_mariadb_pkgs:
  pkg.installed:
    - pkgs:
      - MariaDB-server
      - MariaDB-client
    - require:
      - configure_mariadb_repo

ensure_mariadb_running:
  service.running:
    - name: mariadb
    - enable: True
    - require:
      - install_mariadb_pkgs

secure_mysql:
  cmd.script:
    - name: secure_db.sh "{{ pillar['root_password'] }}"
    - source: salt://mariadb/secure_db.sh
    - require:
      - ensure_mariadb_running
    - unless: mysql --user=root --password='{{ pillar['root_password'] }}' --execute="quit"

dmestry_p_local:
  mysql_user.present:
    - name: dmestry_p
    - host: localhost
    - password: "{{ pillar['root_password'] }}"
    - connection_user: root
    - connection_pass: "{{ pillar['root_password'] }}"
    - connection_charset: utf8
    - require:
      - secure_mysql

dmestry_p_client:
  mysql_user.present:
    - name: dmestry_p
    - host: '%'
    - password: "{{ pillar['root_password'] }}"
    - connection_user: root
    - connection_pass: "{{ pillar['root_password'] }}"
    - connection_charset: utf8
    - require:
      - secure_mysql

dmestry_p_db:
  mysql_user.present:
    - name: dmestry_p
    - host: {{ pillar['db_hostname'] }}
    - password: "{{ pillar['root_password'] }}"
    - connection_user: root
    - connection_pass: "{{ pillar['root_password'] }}"
    - connection_charset: utf8
    - require:
      - secure_mysql

ensure_db_present:
  mysql_database.present:
    - name: blog
    - connection_user: root
    - connection_pass: {{ pillar['root_password'] }}
    - require:
      - pkg: MySQL-python

mysql_user_grants:
   mysql_grants.present:
    - grant: all privileges
    - database: '*.*'
    - grant_option: true
    - user: 'dmestry_p'
    - host: '%'
    - connection_user: root
    - connection_pass: {{ pillar['root_password'] }}
    - require:
      - ensure_db_present

mysql_user_grants_blog:
   mysql_grants.present:
    - grant: all privileges
    - database: '*.*'
    - grant_option: true
    - user: 'dmestry_p'
    - host: {{ pillar['db_hostname'] }}
    - connection_user: root
    - connection_pass: {{ pillar['root_password'] }}
    - require:
      - ensure_db_present

mysql_user_grants_localhost:
   mysql_grants.present:
    - grant: all privileges
    - database: '*.*'
    - grant_option: true
    - user: 'dmestry_p'
    - host: 'localhost'
    - connection_user: root
    - connection_pass: {{ pillar['root_password'] }}
    - require:
      - ensure_db_present

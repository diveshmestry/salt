DIR="/var/www/html/wordpress"
if [ -d "$DIR" ]; then
  ### Take action if $DIR exists ###
  echo "directory exists, exiting"
else
  # install wordpress
  sudo wget http://wordpress.org/latest.tar.gz
  sudo tar -xzvf latest.tar.gz
  sudo rsync -avP ~/wordpress /var/www/html/
  sudo chown -R apache:apache /var/www/html/wordpress
  sudo chown -R apache:apache /var/www/html/wordpress/*
  #sudo cd /var/www/html/efs/wordpress
  #sudo find . -type f -exec chmod 664 {} +
  #sudo find . -type d -exec chmod 775 {} +
fi
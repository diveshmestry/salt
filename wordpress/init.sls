download_cli:
  pkg.installed:
    - name: wp-cli

test_wp_cli:
  cmd.run:
    - name: wp --info
    - require:
      - pkg: wp-cli

install_wordpress:
  cmd.script:
    - name: install_wordpress.sh
    - source: salt://wordpress/install_wordpress.sh

copy_wp_config_sample:
  file.copy:
    - name: /var/www/html/wordpress/wp-config.php
    - source: /var/www/html/wordpress/wp-config-sample.php
    - user: apache
    - group: apache
    - mode: 666
    - require:
      - install_wordpress

configure_wp_config_dbname:
  file.replace:
    - name: /var/www/html/wordpress/wp-config.php
    - pattern: {{ 'define( \'DB_NAME\', \'database_name_here\' );' | regex_escape }}
    - repl: "define( 'DB_NAME', '{{  pillar['db_name']  }}' );"
    - require:
      -  copy_wp_config_sample

configure_wp_config_user:
  file.replace:
    - name: /var/www/html/wordpress/wp-config.php
    - pattern: {{ 'define( \'DB_USER\', \'username_here\' );' | regex_escape }}
    - repl: "define( 'DB_USER', '{{  pillar['db_username']  }}' );"
    - require:
      -  copy_wp_config_sample

configure_wp_config_pw:
  file.replace:
    - name: /var/www/html/wordpress/wp-config.php
    - pattern: {{ 'define( \'DB_PASSWORD\', \'password_here\' );' | regex_escape }}
    - repl: "define( 'DB_PASSWORD', '{{  pillar['db_password']  }}' );"
    - require:
      -  copy_wp_config_sample

configure_wp_config_host:
  file.replace:
    - name: /var/www/html/wordpress/wp-config.php
    - pattern: {{ 'define( \'DB_HOST\', \'localhost\' );' | regex_escape }}
    - repl: "define( 'DB_HOST', '{{  pillar['db_hostname']  }}' );"
    - require:
      -  copy_wp_config_sample



#ensure_wordpress:
#  wordpress.installed:
#    - name: /var/www/html/wordpress
#    - title: {{  pillar['wp_title']  }}
#    - user: {{  pillar['wp_username']  }}
#    - admin_user: {{  pillar['wp_username']  }}
#    - admin_email: {{  pillar['wp_email']  }}
#    - admin_password: {{  pillar['wp_password']  }}
#    - url: {{  pillar['wp_url']  }}
#    - require:
#      - pkg: wp-cli

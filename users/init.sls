create_ssh_group:
  group.present:
    - name: ssh
    - system: True

create_sudo_group:
  group.present:
    - name: sudo
    - system: True

{% for user, args in pillar['users'].iteritems() %}

create_{{ user }}_group:
  group.present:
    - name: {{ user }}

{{ user }}:
  user.present:
    - home: {{ args['home'] }}
    - createhome: True
    - shell: /bin/bash
    - gid_from_name: True

{% if 'groups' in args %}
    - groups:
      - {{ args['groups'] }}
{% endif %}

{% if 'password' in args %}
    - password: {{ args['password'] }}
{% endif %}

{% if 'enforce_password' in args %}
    - enforce_password: {{ args['enforce_password'] }}
{% endif %}

{% if 'passwordExpires' in args and args['passwordExpires'] == False %}
disable_password_expiry_{{ user }}:
  cmd.run:
    - name: chage -I -1 -m 0 -M 99999 -E -1 {{ user }}
    - require:
      - {{ user }}
{% endif %}

/home/{{ user }}/.ssh:
  file.directory:
    - user: {{ user }}
    - group : {{ user }}
    - dir_mode: 700
    - file_mode: 600
    - require:
      - {{ user }}
 
/home/{{ user }}/.ssh/authorized_keys:
  file.touch:
    - makedirs: True
    - require:
      - /home/{{ user }}/.ssh

change_{{ user }}_authkeys_perms:
  file.managed:
    - name: /home/{{ user }}/.ssh/authorized_keys
    - user: {{ user }}
    - group: {{ user }}
    - replace: False

{% if 'key.pub' in args and args['key.pub'] == True %}
{{ user }}.pub:
  ssh_auth:
    - present
    - user: {{ user }}
    - enc: ssh-rsa
    - source: salt://users/list/{{ user }}/keys/jumpbox.pub
    - config: '/home/{{ user }}/.ssh/authorized_keys'
{% endif %}

SetAllowedUsersfor_{{ user }}:
  cmd.run:
    - name: sh ModifyAllowedUsers.sh /etc/ssh/sshd_config {{ user }}
    - cwd: /srv/salt/users/scripts

{% endfor %}

restart_ssh_service:
  cmd.run:
    - name: systemctl restart sshd.service

#!/bin/bash

#usage: sh addUserToAllowSSH.sh /path/to/file username
#example: sh addUserToAllowSSH.sh /etc/ssh/sshd_config dmestry

# take file to edit and user to add as inputs
file=$1
user=$2


addUserToConfig () {
       #if file exists and the user exists continue, else, exit
       if [ -e $1 ] && id -u $2 >/dev/null 2>&1
       then
               #if the user is found in the AllowUsers line,exit. Else, add the user to the line
               if  grep '^AllowUsers' $1 | grep $2 >/dev/null
               then
                       #echo "user already there"
                       return 0
               else
                       #echo "adding user"
                       sed -i '/^AllowUsers/ s/$/ '$2'/' $1
                       return 0
               fi
       else
               #echo "file not found or the user does not exist"
               return 1
       fi
}


addUserToConfig $file $user

#if [ $? -eq 0 ]; then
#       #echo "restarting ssh"
#       sudo systemctl restart ssh.service
#       exit 0
#else
#       exit 1
#fi

disable_selinux:
  salt.state:
    - tgt: 'blog*'
    - tgt_type: grain
    - sls:
      - selinux
    - order: 1

#setup_efs:
#  salt.state:
#    - tgt: 'blog*'
#    - tgt_type: grain
#    - sls:
#      - efs
#    - require:
#      - salt: disable_selinux

install_wget:
  salt.state:
    - tgt: 'blog*'
    - tgt_type: grain
    - sls:
      - wget

install_epel:
  salt.state:
    - tgt: 'blog*'
    - tgt_type: grain
    - sls:
      - epel
    - require:
      - salt: install_wget

install_apache:
  salt.state:
    - tgt: 'blog*'
    - tgt_type: grain
    - sls:
      - apache
    - require:
      - salt: install_epel

install_php:
  salt.state:
    - tgt: 'blog*'
    - tgt_type: grain
    - sls:
      - php
    - require:
      - salt: install_apache

install_mariadb:
  salt.state:
    - tgt: 'blog*'
    - tgt_type: grain
    - sls:
      - mariadb.client
    - require:
      - salt: install_php
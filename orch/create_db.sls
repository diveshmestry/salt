install_wget:
  salt.state:
    - tgt: 'blog*'
    - tgt_type: grain
    - sls:
      - wget

install_epel:
  salt.state:
    - tgt: 'blog*'
    - tgt_type: grain
    - sls:
      - epel
    - require:
      - salt: install_wget

install_mariadb:
  salt.state:
    - tgt: 'blog*'
    - tgt_type: grain
    - sls:
      - mariadb
    - require:
      - salt: install_epel

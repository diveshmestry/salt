mod_firewall:
  firewalld.present:
    - name: public
    - services:
      - http
    - ports:
      - 80/tcp
      - 8080/tcp
      - 443/tcp
      - 3306/tcp

firewalld:
  service.running:
    - reload: True
    - watch:
      - firewalld: public


#mod_firewall_http:
#  cmd.run:
#    - name: sudo firewall-cmd --permanent --add-service=http
#
#mod_firewall_8080:
#  cmd.run:
#    - name: sudo firewall-cmd --permanent --zone=public --add-port=8080/tcp
#
#reload_firewall:
#  cmd.run:
#  - name: sudo systemctl reload firewalld
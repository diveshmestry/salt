install_http:
  pkg.installed:
    - name: httpd

start_http:
  service.running:
    - name: httpd
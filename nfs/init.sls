install_nfs_utils:
  pkg.installed:
    - name: nfs-utils

make_mount_dir:
  file.directory:
    - name: /mnt/efs
    - user: root
    - group: root
    - mode: 777

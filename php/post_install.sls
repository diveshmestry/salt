copy_page:
  file.managed:
    - name: /var/www/html/wordpress/index.php
    - source: salt://php/index.php

copy_access_page:
  file.managed:
    - name: /var/www/html/wordpress/.htaccess
    - source: salt://php/.htaccess
    - mode: 777

start_http:
  service.running:
    - name: httpd
    - reload: True

restart_apache:
  cmd.run:
    - name: sudo systemctl restart httpd
    - order: last
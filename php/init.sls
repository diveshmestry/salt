install_php74:
  cmd.script:
    - source: salt://php/install_php74.sh
    - unless: sudo yum list installed | grep php74

install_php_pkgs:
  pkg.installed:
    - pkgs:
      - php-common
      - php-gd
      - php-xml
      - php-mbstring
      - php-mysqlnd
      - php-pdo
      - php-fpm
      - php-pear
      - php
      - php-cli
      - php-devel
      - php-bcmath

start_http:
  service.running:
    - name: httpd

restart_apache:
  cmd.run:
    - name: sudo systemctl restart httpd
    - order: last
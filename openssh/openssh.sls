install_openssh:
  pkg.installed:
    - name: openssh-server

sshd:
  service.running:
    - enable: True
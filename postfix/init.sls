install_postfix:
  pkg.installed:
    - name: postfix

postfix:
  service.running:
    - enable: True
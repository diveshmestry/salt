users:
  dmestry_p:
    shell: /bin/bash
    home: /home/dmestry_p
    password: '$6$78c6UqNBmI1z/vjg$WsQ4i4Hz8I36Xov4bC56nday6ettho4Nfsz8NIpoMsX.FnPZ.rnc9MmMVP.a2oq2OzeAwvtkbvFQbFp1HdsYR1'
    groups: sudo
    key.pub: True
    canSudo: True
    passwordExpires: False
